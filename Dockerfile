FROM golang:1.21-alpine3.19 as builder

WORKDIR /src/app

COPY go.mod go.sum ./
COPY cmd ./cmd
COPY internal ./internal
RUN go build -o server cmd/main.go

FROM debian:bookworm

WORKDIR /root/
COPY --from=builder /src/app ./app

CMD ["./app/server"]