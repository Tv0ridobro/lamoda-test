up:
	docker compose up
.PHONY: up

test:
	export TEST_DATABASE_URL="postgres://postgres:password@localhost:5432/postgres" && \
	go test -cover ./... 
.PHONY: test

run:
	export DATABASE_URL="postgres://postgres:password@localhost:5432/postgres" && \
	export PORT=":8080" && \
	go run cmd/main.go
.PHONY: run