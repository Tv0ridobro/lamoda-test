package main

import (
	"context"
	"errors"
	"lamoda-test/internal/handler"
	"lamoda-test/internal/service"
	"lamoda-test/internal/storage/postgres"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/jackc/pgx"
)

func main() {
	pgxCfg, err := pgx.ParseConnectionString(os.Getenv("DATABASE_URL"))
	if err != nil {
		log.Fatal("can't parse postgres connection string ", err.Error())
		return
	}

	connPool, err := pgx.NewConnPool(pgx.ConnPoolConfig{ConnConfig: pgxCfg})
	if err != nil {
		log.Fatal("can't create connection pool ", err.Error())
		return
	}
	defer connPool.Close()

	s := service.New(handler.New(
		postgres.NewReservationStorage(connPool),
	))

	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	ready := make(chan struct{})
	go func() {
		<-ctx.Done()
		stop()

		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		if err := s.Shutdown(ctx); err != nil {
			log.Println("shutdown server", err.Error())
		}
		ready <- struct{}{}
	}()

	if err := s.Run(context.Background()); !errors.Is(err, http.ErrServerClosed) {
		log.Println("http server listen and serve", err.Error())
	}
	<-ready
}
