package handler

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"lamoda-test/internal/storage"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

type Handler struct {
	router       http.Handler
	reservations storage.Reservation
}

func (h Handler) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	h.router.ServeHTTP(writer, request)
}

func New(rv storage.Reservation) Handler {
	h := Handler{
		reservations: rv,
	}

	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)

	r.Route("/warehouses", func(r chi.Router) {
		r.Route("/{name}", func(r chi.Router) {
			r.Get("/count", h.CountProducts)

			r.Route("/reservations", func(r chi.Router) {
				r.Post("/", h.ReserveReservation)
				r.Delete("/", h.ReleaseReservation)
			})
		})
	})

	h.router = r

	return h
}

func (h Handler) ReserveReservation(rw http.ResponseWriter, r *http.Request) {
	warehouseName := chi.URLParam(r, "name")

	var ids []string
	if err := json.NewDecoder(r.Body).Decode(&ids); err != nil {
		h.HandleError(rw, r, err)
		return
	}

	if !chechUnique(ids) {
		h.HandleError(rw, r, fmt.Errorf("%w array has repeatable elements", ErrValidation))
		return
	}

	if err := h.reservations.ReserveProducts(r.Context(), warehouseName, ids); err != nil {
		h.HandleError(rw, r, err)
		return
	}

	rw.WriteHeader(http.StatusCreated)
}

func (h Handler) ReleaseReservation(rw http.ResponseWriter, r *http.Request) {
	warehouseName := chi.URLParam(r, "name")

	var ids []string
	if err := json.NewDecoder(r.Body).Decode(&ids); err != nil {
		h.HandleError(rw, r, err)
		return
	}

	if !chechUnique(ids) {
		h.HandleError(rw, r, fmt.Errorf("%w array has repeatable elements", ErrValidation))
		return
	}

	if err := h.reservations.ReleaseProducts(r.Context(), warehouseName, ids); err != nil {
		h.HandleError(rw, r, err)
		return
	}

	rw.WriteHeader(http.StatusOK)
}

func (h Handler) CountProducts(rw http.ResponseWriter, r *http.Request) {
	warehouseName := chi.URLParam(r, "name")

	count, err := h.reservations.CountProducts(r.Context(), warehouseName)
	if err != nil {
		h.HandleError(rw, r, err)
		return
	}

	if _, err = rw.Write([]byte(strconv.Itoa(count))); err != nil {
		h.HandleError(rw, r, err)
		return
	}
}

func (h *Handler) HandleError(rw http.ResponseWriter, r *http.Request, err error) {
	var jsonSyntaxErr *json.SyntaxError
	var jsonTypeErr *json.UnmarshalTypeError

	switch {
	case errors.As(err, &jsonSyntaxErr) || errors.As(err, &jsonTypeErr) || errors.Is(err, io.ErrUnexpectedEOF):
		rw.WriteHeader(http.StatusBadRequest)
	case errors.Is(err, storage.ErrDoesntExist):
		rw.WriteHeader(http.StatusNotFound)
	case errors.Is(err, storage.ErrNotAvailable):
		rw.WriteHeader(http.StatusBadRequest)
	case errors.Is(err, ErrValidation):
		rw.WriteHeader(http.StatusBadRequest)
	case errors.Is(err, storage.ErrAlreadyExist):
		rw.WriteHeader(http.StatusBadRequest)
	default:
		rw.WriteHeader(http.StatusInternalServerError)
	}
	rw.Write([]byte(err.Error()))
}

func chechUnique(strings []string) bool {
	m := make(map[string]struct{}, len(strings))
	for i := range strings {
		m[strings[i]] = struct{}{}
	}

	return len(m) == len(strings)
}
