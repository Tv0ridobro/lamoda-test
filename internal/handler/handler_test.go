package handler

import (
	"context"
	"errors"
	"lamoda-test/internal/storage"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestChechUnique(t *testing.T) {
	tests := []struct {
		s    []string
		isOk bool
	}{
		{[]string{}, true},
		{[]string{"1"}, true},
		{[]string{""}, true},
		{[]string{"1", "2"}, true},
		{[]string{"1", ""}, true},
		{[]string{"1", "1", "1", "1", "1"}, false},
		{[]string{"", ""}, false},
		{[]string{"1", "1"}, false},
		{[]string{"1", "2", "6", "-1", "14141", "131313231"}, true},
		{[]string{"1", "2", "6", "-1", "14141", "131313231", "1"}, false},
		{[]string{"1", "2", "1", "6", "-1", "14141", "131313231", "1"}, false},
	}

	for i := range tests {
		ok := chechUnique(tests[i].s)
		assert.Equal(t, tests[i].isOk, ok)
	}
}

type mockReservationStorage struct {
	mock.Mock
}

func (s *mockReservationStorage) ReserveProducts(ctx context.Context, warehouseName string, ids []string) error {
	args := s.Called(ctx, warehouseName, ids)
	return args.Error(0)
}

func (s *mockReservationStorage) ReleaseProducts(ctx context.Context, warehouseName string, ids []string) error {
	args := s.Called(ctx, warehouseName, ids)
	return args.Error(0)
}

func (s *mockReservationStorage) CountProducts(ctx context.Context, warehouseName string) (int, error) {
	args := s.Called(ctx, warehouseName)
	return args.Int(0), args.Error(1)
}

func TestReserveProducts(t *testing.T) {
	m := new(mockReservationStorage)
	handler := New(m)

	rw := httptest.NewRecorder()
	r := httptest.NewRequest("POST", "/warehouses/1/reservations", strings.NewReader(`["1", "2"]`))

	m.On("ReserveProducts", mock.Anything, "1", []string{"1", "2"}).Return(nil)
	handler.ServeHTTP(rw, r)
	m.AssertCalled(t, "ReserveProducts", mock.Anything, "1", []string{"1", "2"})

	assert.Equal(t, rw.Code, 201)
}

func TestReleaseProducts(t *testing.T) {
	m := new(mockReservationStorage)
	handler := New(m)

	rw := httptest.NewRecorder()
	r := httptest.NewRequest("DELETE", "/warehouses/10/reservations", strings.NewReader(`["1", "2", "3"]`))

	m.On("ReleaseProducts", mock.Anything, "10", []string{"1", "2", "3"}).Return(nil)
	handler.ServeHTTP(rw, r)
	m.AssertCalled(t, "ReleaseProducts", mock.Anything, "10", []string{"1", "2", "3"})

	assert.Equal(t, rw.Code, 200)
}

func TestCountProducts(t *testing.T) {
	m := new(mockReservationStorage)
	handler := New(m)

	rw := httptest.NewRecorder()
	r := httptest.NewRequest("GET", "/warehouses/1/count", strings.NewReader(""))

	m.On("CountProducts", mock.Anything, "1").Return(12, nil)
	handler.ServeHTTP(rw, r)
	m.AssertCalled(t, "CountProducts", mock.Anything, "1")

	assert.Equal(t, rw.Code, 200)
	assert.Equal(t, rw.Body.String(), "12")
}

func TestReserveProductsErrors(t *testing.T) {
	m := new(mockReservationStorage)
	handler := New(m)

	rw := httptest.NewRecorder()
	r := httptest.NewRequest("POST", "/warehouses/1/reservations", strings.NewReader(`["1", "2"`))

	handler.ServeHTTP(rw, r)
	assert.Equal(t, rw.Code, 400)

	rw = httptest.NewRecorder()
	r = httptest.NewRequest("POST", "/warehouses/1/reservations", strings.NewReader(`["1", "1"]`))

	handler.ServeHTTP(rw, r)
	assert.Equal(t, rw.Code, 400)
	assert.Contains(t, rw.Body.String(), ErrValidation.Error())

	rw = httptest.NewRecorder()
	r = httptest.NewRequest("POST", "/warehouses/1000/reservations", strings.NewReader(`["1", "2"]`))

	m.On("ReserveProducts", mock.Anything, "1000", []string{"1", "2"}).Return(storage.ErrNotAvailable)
	handler.ServeHTTP(rw, r)
	m.AssertCalled(t, "ReserveProducts", mock.Anything, "1000", []string{"1", "2"})
	assert.Equal(t, rw.Code, 400)
	assert.Contains(t, rw.Body.String(), storage.ErrNotAvailable.Error())

	rw = httptest.NewRecorder()
	r = httptest.NewRequest("POST", "/warehouses/10/reservations", strings.NewReader(`["1", "2"]`))

	m.On("ReserveProducts", mock.Anything, "10", []string{"1", "2"}).Return(storage.ErrAlreadyExist)
	handler.ServeHTTP(rw, r)
	m.AssertCalled(t, "ReserveProducts", mock.Anything, "10", []string{"1", "2"})
	assert.Equal(t, rw.Code, 400)
	assert.Contains(t, rw.Body.String(), storage.ErrAlreadyExist.Error())
}

func TestReleaseProductsError(t *testing.T) {
	m := new(mockReservationStorage)
	handler := New(m)

	rw := httptest.NewRecorder()
	r := httptest.NewRequest("DELETE", "/warehouses/name/reservations", strings.NewReader(`["4", "4"]`))

	handler.ServeHTTP(rw, r)
	assert.Equal(t, rw.Code, 400)
	assert.Contains(t, rw.Body.String(), ErrValidation.Error())

	rw = httptest.NewRecorder()
	r = httptest.NewRequest("DELETE", "/warehouses/name/reservations", strings.NewReader(`[4, 4]`))

	handler.ServeHTTP(rw, r)
	assert.Equal(t, rw.Code, 400)

	rw = httptest.NewRecorder()
	r = httptest.NewRequest("DELETE", "/warehouses/1000/reservations", strings.NewReader(`["1", "2"]`))

	m.On("ReleaseProducts", mock.Anything, "1000", []string{"1", "2"}).Return(storage.ErrDoesntExist)
	handler.ServeHTTP(rw, r)
	m.AssertCalled(t, "ReleaseProducts", mock.Anything, "1000", []string{"1", "2"})
	assert.Equal(t, rw.Code, 404)
	assert.Contains(t, rw.Body.String(), storage.ErrDoesntExist.Error())
}

func TestCountProductsError(t *testing.T) {
	m := new(mockReservationStorage)
	handler := New(m)

	rw := httptest.NewRecorder()
	r := httptest.NewRequest("GET", "/warehouses/1/count", strings.NewReader(""))

	m.On("CountProducts", mock.Anything, "1").Return(0, errors.New("unknown error"))
	handler.ServeHTTP(rw, r)
	m.AssertCalled(t, "CountProducts", mock.Anything, "1")

	assert.Equal(t, rw.Code, 500)
}
