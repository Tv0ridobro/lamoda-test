package service

import (
	"context"
	"net/http"
	"os"
)

type Service struct {
	server *http.Server
}

func (s *Service) Run(_ context.Context) error {
	return s.server.ListenAndServe()
}

func (s *Service) Shutdown(ctx context.Context) error {
	return s.server.Shutdown(ctx)
}

func New(h http.Handler) Service {
	s := Service{
		server: &http.Server{
			Handler: h,
			Addr:    os.Getenv("PORT"),
		},
	}

	return s
}
