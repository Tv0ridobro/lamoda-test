package postgres

import (
	"context"
	"errors"
	"fmt"
	"lamoda-test/internal/storage"

	"github.com/jackc/pgx"
)

type ReservationStorage struct {
	conn *pgx.ConnPool
}

func NewReservationStorage(c *pgx.ConnPool) ReservationStorage {
	return ReservationStorage{conn: c}
}

func (r ReservationStorage) ReserveProducts(ctx context.Context, warehouseName string, ids []string) error {
	t, err := r.conn.BeginEx(ctx, nil)
	if err != nil {
		return nil
	}
	defer t.Rollback()

	if err = chechAvailable(ctx, t, warehouseName); err != nil {
		return err
	}

	for i := range ids {
		_, err := t.ExecEx(ctx, "INSERT INTO reservation VALUES ($1, $2)", nil, warehouseName, ids[i])

		if err != nil {
			pgxErr := pgx.PgError{}

			if errors.As(err, &pgxErr) {
				switch pgxErr.ConstraintName {
				case "reservation_pkey":
					return fmt.Errorf("%w : reservation with id = %s", storage.ErrAlreadyExist, ids[i])
				case "reservation_warehouse_name_fkey":
					return fmt.Errorf("%w : warehouse with name = %s", storage.ErrDoesntExist, warehouseName)
				case "reservation_product_id_fkey":
					return fmt.Errorf("%w : reservation with id = %s", storage.ErrDoesntExist, ids[i])
				default:
					return err
				}
			}

			return err
		}
	}

	if err := t.CommitEx(ctx); err != nil {
		return err
	}

	return nil
}

func (r ReservationStorage) ReleaseProducts(ctx context.Context, warehouseName string, ids []string) error {
	t, err := r.conn.BeginEx(ctx, nil)
	if err != nil {
		return nil
	}
	defer t.Rollback()

	if err = chechAvailable(ctx, t, warehouseName); err != nil {
		return err
	}

	for i := range ids {
		c, err := t.ExecEx(ctx, "DELETE FROM reservation WHERE warehouse_name = $1 AND product_id = $2", nil, warehouseName, ids[i])
		if err != nil {
			return err
		}
		if c.RowsAffected() == 0 {
			return fmt.Errorf("%w : reservation with id = %s and warehouse = %s", storage.ErrDoesntExist, ids[i], warehouseName)
		}
	}

	if err := t.CommitEx(ctx); err != nil {
		return err
	}

	return nil
}

func (r ReservationStorage) CountProducts(ctx context.Context, warehouseName string) (int, error) {
	row := r.conn.QueryRowEx(ctx, "SELECT count(*) FROM reservation WHERE warehouse_name = $1", nil, warehouseName)

	var count int
	if err := row.Scan(&count); err != nil {
		return 0, err
	}

	return count, nil
}

func chechAvailable(ctx context.Context, t *pgx.Tx, warehouseName string) error {
	row := t.QueryRow("SELECT is_available FROM warehouse WHERE name = $1;", warehouseName)

	var IsAvailable bool
	if err := row.Scan(&IsAvailable); err != nil {
		switch {
		case errors.Is(err, pgx.ErrNoRows):
			return fmt.Errorf("%w : warehouse with name = %s", storage.ErrDoesntExist, warehouseName)
		default:
			return err
		}
	}

	if !IsAvailable {
		return storage.ErrNotAvailable
	}

	return nil
}
