package postgres

import (
	"context"
	"lamoda-test/internal/storage"
	"log"
	"os"
	"testing"

	"github.com/jackc/pgx"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

var s ReservationStorage

func TestMain(m *testing.M) {
	cfg, err := pgx.ParseConnectionString(os.Getenv("TEST_DATABASE_URL"))
	if err != nil {
		log.Fatalln(err.Error())
		return
	}

	connPool, err := pgx.NewConnPool(pgx.ConnPoolConfig{ConnConfig: cfg})
	if err != nil {
		log.Fatalln(err.Error())
		return
	}
	defer connPool.Close()

	s = NewReservationStorage(connPool)
	m.Run()
}

func TestReservationStorage(t *testing.T) {
	testData(t)
	defer cleanData(t)
	ctx := context.Background()

	count, err := s.CountProducts(ctx, "1")
	assert.Nil(t, err)
	assert.Equal(t, 0, count)

	count, err = s.CountProducts(ctx, "7")
	assert.Nil(t, err)
	assert.Equal(t, 0, count)

	err = s.ReserveProducts(ctx, "1", []string{"1", "2", "3", "4"})
	assert.Nil(t, err)
	err = s.ReserveProducts(ctx, "2", []string{"9", "5", "3"})
	assert.Nil(t, err)
	err = s.ReserveProducts(ctx, "3", []string{"1", "6", "8", "7"})
	assert.Nil(t, err)

	count, err = s.CountProducts(ctx, "1")
	assert.Nil(t, err)
	assert.Equal(t, 4, count)

	err = s.ReleaseProducts(ctx, "1", []string{"1", "4"})
	assert.Nil(t, err)

	count, err = s.CountProducts(ctx, "1")
	assert.Nil(t, err)
	assert.Equal(t, 2, count)

	err = s.ReleaseProducts(ctx, "1", []string{"2", "3"})
	assert.Nil(t, err)

	count, err = s.CountProducts(ctx, "1")
	assert.Nil(t, err)
	assert.Equal(t, 0, count)

	count, err = s.CountProducts(ctx, "2")
	assert.Nil(t, err)
	assert.Equal(t, 3, count)

	count, err = s.CountProducts(ctx, "hello")
	// cant really understand if there are really no rows or no reservations
	assert.Nil(t, err)
	assert.Equal(t, 0, count)

	err = s.ReleaseProducts(ctx, "100", []string{"2", "3"})
	assert.ErrorIs(t, err, storage.ErrDoesntExist)

	err = s.ReserveProducts(ctx, "1", []string{})
	assert.Nil(t, err)

	err = s.ReleaseProducts(ctx, "1", []string{})
	assert.Nil(t, err)

	err = s.ReserveProducts(ctx, "4", []string{"1"})
	assert.Nil(t, err)

	err = s.ReserveProducts(ctx, "4", []string{"1"})
	assert.ErrorIs(t, err, storage.ErrAlreadyExist)

	err = s.ReleaseProducts(ctx, "4", []string{"1", "1"})
	assert.ErrorIs(t, err, storage.ErrDoesntExist)

	err = s.ReserveProducts(ctx, "4", []string{"2", "2"})
	assert.ErrorIs(t, err, storage.ErrAlreadyExist)

	err = s.ReserveProducts(ctx, "5", []string{"hello"})
	assert.ErrorIs(t, err, storage.ErrDoesntExist)

	err = s.ReserveProducts(ctx, "7", []string{"1", "2"})
	assert.ErrorIs(t, err, storage.ErrNotAvailable)
}

func testData(t *testing.T) {
	t.Helper()
	_, err := s.conn.Exec(
		`INSERT INTO warehouse VALUES
			('1', true),
    		('2', true),
			('3', true),
			('4', true),
			('5', true),
			('6', true),
			('7', false);`)
	require.Nil(t, err)

	_, err = s.conn.Exec(
		`INSERT INTO product VALUES
			('1', 'shirt', 1, 'S'),
			('2', 'pants', 20, 'S'),
			('3', 'dress', 12, 'L'),
			('4', 'hoodie', 14, 'M'),
			('5', 'shorts', 3, 'S'),
			('6', 'cap', 4, 'S'),
			('7', 'jacket', 22, 'M'),
			('8', 'jeans', 200, 'M'),
			('9', 't-shirt', 1, 'S'),
			('10', 'sneakers', 7, 'L');`)
	require.Nil(t, err)
}

func cleanData(t *testing.T) {
	t.Helper()
	_, err := s.conn.Exec(
		`TRUNCATE table reservation, warehouse, product;`)
	require.Nil(t, err)
}
