package storage

import (
	"context"
	"errors"
)

var ErrDoesntExist error = errors.New("entity not found")
var ErrAlreadyExist error = errors.New("entity already exist")
var ErrNotAvailable error = errors.New("warehouse not available")

type Reservation interface {
	ReserveProducts(ctx context.Context, warehouseName string, ids []string) error
	ReleaseProducts(ctx context.Context, warehouseName string, ids []string) error
	CountProducts(ctx context.Context, warehouseName string) (int, error)
}
