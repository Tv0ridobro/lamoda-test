CREATE TABLE IF NOT EXISTS warehouse (
    name text PRIMARY KEY NOT NULL,
    is_available boolean NOT NULL
);

CREATE TABLE IF NOT EXISTS product (
    id text PRIMARY KEY NOT NULL,
    name text NOT NULL,
    amount integer,
    size text
);

CREATE TABLE IF NOT EXISTS reservation (
    warehouse_name text REFERENCES warehouse,
    product_id text REFERENCES product,
    PRIMARY KEY (warehouse_name, product_id)
);
