INSERT INTO
    product
VALUES
    ('1', 'shirt', 1, 'S'),
    ('2', 'pants', 20, 'S'),
    ('3', 'dress', 12, 'L'),
    ('4', 'hoodie', 14, 'M'),
    ('5', 'shorts', 3, 'S'),
    ('6', 'cap', 4, 'S'),
    ('7', 'jacket', 22, 'M'),
    ('8', 'jeans', 200, 'M'),
    ('9', 't-shirt', 1, 'S'),
    ('10', 'sneakers', 7, 'L');

INSERT INTO
    warehouse
VALUES
    ('1', true),
    ('2', true),
    ('3', true),
    ('4', true),
    ('5', true),
    ('6', true),
    ('7', false);